id: hands-on-demo
summary: GitLab Pages (FR)
authors: @k33g_org

# 🌍 GitLab Pages
> Main content

## 👋 Bonjour 😃

Les **GitLab Pages** sont un moyen simple pour publier des pages statiques à partir d'un projet GitLab. Nous allons découvrir comment les utiliser en 3 exercices (il y a un projet par exercice).
Vous n'en aurez pas besoin aujourd'hui, mais vous trouverez la documentation des GitLab Pages ici:
- [https://docs.gitlab.com/ee/user/project/pages/](https://docs.gitlab.com/ee/user/project/pages/)

Positive
: Pour chacun des exercices nous utiliserons le **Web IDE** de GitLab et les shared runners, vous n'avez donc rien à installer.

Negative
: 🖐 Pour le 1er exercice, le formateur peut le faire en même temps que les apprenants pour montrer comment utiliser le **Web IDE**

<!-- ------------------------ -->
## 📝 1ère page

Cet exercice vous présente la façon **la plus simple** de publier une page web statique avec les GitLab Pages.

Pour pouvoir publier une page statique à partir d'un projet GitLab, la fonctionnalité des GitLab Pages "a besoin de trouver" à minima une page `index.html` dans un répertoire `public`, et pour déclencher la fonctionnalité GitLab Pages, il vous faudra un job de CI nommé `pages` avec le répertoire `public` dans la liste des `paths` des `artifacts` *(ne vous inquiétez pas, nous allons voir cela un peu plus loin)*.

Positive
: Positionnez vous dans votre groupe de travail (le nom est de la forme `grp_<votre-handle-gitlab>`)

Dans votre groupe de travail, vous avez 3 projets:
- `1st-Page`
- `2nd-Page`
- `3rd-Page`

"Allez" dans `1st-Page` et ouvrez le **Web IDE**

### Créez la page web

- Créez un répertoire `public`
- Dans le répertoire `public` créez une page `index.html`
- Ajoutez un peu de code html dans `index.html` : `<h1>👋 Hello World 🌍</h1>`

### Créez le pipeline de publication

- Ajoutez un fichier `.gitlab-ci.yml` à votre projet avec le contenu suivant:

```yaml
pages:
  artifacts:
    paths:
      - public
  script:
    echo "publishing"
```

- Committez votre code source (sur la branche principale **`main`**), cela va déclencher un pipeline avec 2 stages (Test & Deploy):

![image_caption](1st-pipeline.png)

### 🎉 Votre page est publiée

- Une fois le pipeline terminé, allez dans la rubriques **"Settings"** puis **"Pages"** de votre projet
- Vous obtenez le lien pour accéder à votre page (générallement de la forme: `https://tanooki-workshops.gitlab.io/workshops/gitlab-pages-fr/demo`)

Positive
: 👋 Si vous souhaitez quelque chose qui ressemble plus à `https://group-name.gitlab.io`, vous devez, dans votre groupe `group-name` *(qui doit être un groupe de 1er niveau)* créer un **projet** avec le nom `group-name.gitlab.io` et utiliser ce projet pour votre site web statique.


<!-- ------------------------ -->
## 📝 2ème page avec Eleventy

Les **GitLab Pages** sont très utiles pour héberger le résultat de générateurs de sites statiques (VuePress par exemple). Nous allons essayer le moteur de génération de sites statiques **[Eleventy](https://www.11ty.dev/)**. Cette fois-ci, positionnez vous dans le projet `2nd-Page` et ouvrez le **Web IDE**.

- Vous avez toute la structure nécessaire. 
- Le fichier de configuration précisant la source et la cible est le fichier `.eleventy.js`

Pour publier votre site, il suffit d'utiliser les commandes suivantes:

```bash
npm install
npx @11ty/eleventy
```

La commande `npx @11ty/eleventy` va générer le site statique dans le répertoire `public` (le répertoire est créé par la commande).

### Créez le pipeline de publication

Donc:

- Ajoutez un fichier `.gitlab-ci.yml` à votre projet avec le contenu suivant:

```yaml
image: node:slim

pages:
```

- Complétez le code du pipeline pour pouvoir déployer votre application
- Committez votre code source (sur la branche principale **`main`**)
- Allez "voir" votre nouvelle page (pour retrouver le lien, allez dans la rubriques **"Settings"** puis **"Pages"** de votre projet)


<!-- ------------------------ -->
## 📝 3ème page (Eleventy): Rewiew App

Il est possible d'utiliser le principe des **reviews applications** avec les **GitLab Pages**. Les **reviews applications** permettent de définir et initialiser un nouvel environnement de déploiement à partir d'une **"feature branch"** (par exemple déployer une nouvelle VM). C'est pratique pour tester un développement. Et enfin, une fois que vous mergez votre **"feature branch"**, vous pouvez déclencher la suppression de cet environnement de test.

Negative
: Cela est possible avec un "petit hack", mais c'est bien pratique

Positive
: Plus d'informations sur les **reviews applications** par ici: [https://docs.gitlab.com/ee/ci/review_apps/](https://docs.gitlab.com/ee/ci/review_apps/)

### Les environnements (`environment`)

- Cette fois-ci, positionnez vous dans le projet `3rd-Page` et ouvrez le **Web IDE**. 
- Ajoutez un fichier `.gitlab-ci.yml` à votre projet avec le contenu suivant:

```yaml
image: node:slim

stages:
  - publish

pages:
  stage: publish
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  environment:
    name: ${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    url: ${CI_PAGES_URL}
  script:
    - echo "publishing"
    - npm install
    - npx @11ty/eleventy
```

- Committez votre code source (sur la branche principale **`main`**)
- Allez "voir" votre nouvelle page (pour retrouver le lien, allez dans la rubriques **"Settings"** puis **"Pages"** de votre projet)

Positive
: 👋 La clé `environment` permet de définir un environnement avec un nom et une url lié à votre application web pour une branche donnée

Si vous allez dans le menu (à gauche) et que vous sélectionnez **Operations**, puis **Environments** vous aller voir une liste avec votre nouvel environnement:

![image_caption](1st-environment.png)

Positive
: La flêche ↗️ à droite est un lien vers votre application déployée (vous pouvez essayer de cliquer dessus)

### Activons la Review Application

- Pour activer le mode "review application", **ajoutez** 2 nouveaux jobs avec le code suivant dans votre pipeline (donc dans `.gitlab_ci.yml`):

```yaml
deploy:preview:
  stage: publish
  variables:
    CI_CURRENT_NAMESPACE: "current_workshop_name/grp_<your-gitlab-handle>" 
    # ex: "gitlab-pages-fr/grp_k33g/"
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_MERGE_REQUEST_IID
  environment:
    name: ${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    url: https://${CI_PROJECT_ROOT_NAMESPACE}.gitlab.io/-/${CI_CURRENT_NAMESPACE}/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public/index.html
    on_stop: remove:preview
  script:
    - echo "publishing"
    - npm install
    - npx @11ty/eleventy

remove:preview:
  stage: publish
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  environment:
    name: ${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    action: stop
  allow_failure: true
  script:
    - echo "remove"

```

Negative
: 🖐 faites bien attention à modifier la valeur de la variable `CI_CURRENT_NAMESPACE` (quelque chose de la forme `le_nom_de_ce_workshop/grp_<votre-handle-gitlab>`)

Positive
: 🎉 le **hack**, c'est de simuler un environement de déploiement avec les artefacts (`artifacts`) publiés dans `public` avec  `url` qui va pointer sur `/artifacts/public/index.html` *(vous pourrez aller vérifier an parcourant les artefacts générés par le job)*

Negative
: 🖐 le mot-clé `on_stop` avec la valeur `remove:preview` est **important**, il permet de déclencher le job de suppression des environnements `remove:preview`. Dans notre cas nous ne ferons que supprimer l'environnement de la liste, mais dans la vraie vie, il faudra lancer les commandes ou scripts de suppression de votre environnement (par exemple un `kubectl delete` si vous déployez sur un cluster Kubernetes). Si vous ne faites pas ça vous allez multiplier les environnements.

Negative
: 🖐 le job de suppression doit toujours être en `manual`

Maintenant, committez votre code source *(sur la branche principale)* avec `[skip ci]` dans le message de commit pour éviter de déclencher le pipeline

### Modifiez `index.md` & Faites une Merge Request

Pour tester la **review application**,

- Modifiez le contenu de `/website/index.md`, **toujours avec le Web IDE**
- Committez votre code, mais en faisant cette fois ci une **Merge Request**

Positive
: 🖐 lorsque que vous clickez sur le bouton bleu **Commit...**, sélectionnez l'option **Create a new branch**, laissez cochée la checkbox **Start a new merge request** et enfin, clickez sur le bouton vert **Commit**

<!--![image_caption](1st-commit.png)-->

#### Vous "arrivez" donc sur l'écran de création de merge request

![image_caption](mr-first-part.png)

Scrollez vers le bas et cliquez sur le bouton bleu **Create merge request**

![image_caption](mr-second-part.png)

#### Vous avez donc déclenché le déploiement de votre "review app"

![image_caption](mr-deploy-first-part.png)

Patientez un peu jusqu'à la fin du pipeline. Vous pouvez voir un nouveau bouton **View app** qui vous permet d'accéder à votre **"review app"** (Essayez 😃)

![image_caption](mr-deploy-second-part.png)

#### Allez voir la liste des environnements

![image_caption](environments-01.png)

Positive
: 🎉 maintenant retournez dans votre merge request et mergez. **Cela va déclencher la suppression de votre environnement de test (le job `remove:preview`)** et le job de déploiement des GitLab Pages.

![image_caption](mr-deploy-third-part.png)

Patientez un peu jusqu'à la fin du pipeline. Puis retournez dans la liste des environnements; vous pouvez voir qu'il ne resque plus que votre environnement de production:

![image_caption](environments-02.png)

**🎉🚀 vous pouvez donc allez vérifier que vos modifications ont bien été déployée. 👏**

*THE END 👋*

<!-- ------------------------ -->
## ✋ Informations

Quelques liens sur des billets de blogs que j'avais écrits sur le sujet:
- [Faites pointer votre nom de domaine sur vos GitLab Pages quand vous êtes chez Gandi](https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/39)
- [GitLab Pages faciles](https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/21)



